## Views

We use views just for mail templates. Don't use views for anything else. Use Sencha instead to handle all of the UI.
