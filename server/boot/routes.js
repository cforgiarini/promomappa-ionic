module.exports = function(server) {
  var config = require('../../common/appConfig.js');
  var _ = require('lodash');
  var fs = require('fs');
  var path = require('path');
  var datasources = require('../datasources');
  var app = require(__dirname + '/../../server/server');
  var htmlTemplates = require(__dirname +'/../../resources/htmlTemplate');

   var router = server.loopback.Router();
  //verified
  app.get('/users/changepwd', function(req, res) {
    res.render('password-reset', {
      id: req.query.id,
      token: req.query.token,
      url: req.query.url
    });
  });

  app.post('/reset-password', function(req, res) {
      var esito;
      var User = app.models.user;
      User.findById(req.body.userId, function(err, user) {
        if (err) {
          esito = {
            'data': [{
              'msg': err
            }],
            'success': false,
            'count': 1
          };
          return res.status(200).json(esito);
        } else {
          user.updateAttributes({
            'password': req.body.password,
            'lastPasswordChange': new Date()
          }, function(err) {
            if (err) {
              esito = {
                'data': [{
                  'msg': err
                }],
                'success': false,
                'count': 1
              };
              return res.status(200).json(esito);
            } else {
              esito = {
                'data': [{
                  'msg': 'cambio password terminato correttamente'
                }],
                'success': true,
                'count': 1
              };
              return res.render('passwordchanged');
            }

          });
        }

      });
    }

  );

  app.get('/user/verified', function(req, res) {
    res.render('verified', config);
  });



}