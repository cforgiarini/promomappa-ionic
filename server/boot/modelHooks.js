/**
 * This file defines trigger on model/REST operations
 * a.k.a. remoteHooks
 *
 * remoteHooks are
 * `beforeRemote( methodName, function( ctx, modelInstance, next) {...next() })`
 * `afterRemote( methodName, function( ctx, modelInstance, next) { ...next() })`
 * `afterRemoteError( methodName, function( ctx, next) { ... next(); })`
 * Where:
 * * _modelName_ is the name of the model to which the remote hook is attached.
 * * _methodName_ is the name of the method that triggers the remote hook.
     This may be a custom remote method or a standard CRUD method inherited
     from PersistedModel.
     It may include wildcards to match more than one method (see below).
 * * _ctx_ is the context object. (notably it embeds expressjs req/res as props)
 * * _modelInstance_ is the affected model instance.
 * Use an asterisk '*' in methodName to match any character. Use '*.*' to match
 * any static method; use 'prototype.*' to match any instance method.
 *
 * An example:
 *
 *   User.beforeRemote('create', function (ctx, modelInstance, next) {
 *     var userData = ctx.req.body;
 *     User.findOne({
 *       where: {
 *         email: userData.email
 *       }
 *     }, function (err, result) {
 *       if(err) {
 *         return next(err);
 *       } else if (result != null) {
 *         return next(new Error('Utente già registrato'));
 *       } else {
 *         return next();
 *       }
 *     });
 *   });
 **/

module.exports = function(app) {
 var ALL = app.remotes();
 /*
   Implementare all'occorrenza
 */
   ALL.after('**', function(ctx, next) {
      next();
  });
  ALL.before('**', function(ctx, next) {
      next();
  });
}