'use strict';
var app = require(__dirname + '/../../server/server');
var googleParam = require('../../configuration/param');
var _ = require('lodash');
var APIConnector: any;
let apiKey: string = googleParam.google.apiKey;
module.exports = function(Google: any) {

    Google.beforeRemote("**", function(ctx: any, modelInstance: any, next: any) {
        next();
    });
    let isStatic = true;

    Google.disableRemoteMethod('count', isStatic);
    Google.disableRemoteMethod('deleteById', isStatic);
    Google.disableRemoteMethod('create', isStatic);
    Google.disableRemoteMethod('upsert', isStatic);
    Google.disableRemoteMethod('updateAll', isStatic);
    Google.disableRemoteMethod('findOne', isStatic);
    Google.disableRemoteMethod('find', isStatic);
    Google.disableRemoteMethod('findById', isStatic);
    Google.disableRemoteMethod('replaceOrCreate', isStatic);
    Google.disableRemoteMethod('invoke', isStatic);
    Google.disableRemoteMethod('description', isStatic);
    Google.disableRemoteMethod('replaceById', isStatic);
    Google.disableRemoteMethod('upsertWithWhere', isStatic);
    Google.disableRemoteMethod('__exists__', isStatic);
    Google.disableRemoteMethod("__update-__", isStatic);
    Google.disableRemoteMethod('createChangeStream', isStatic);

    APIConnector = app.dataSources.GoogleDS.connector;
    APIConnector.observe('before execute', function(ctx: any, next: any) {
        let operation: string = ctx.req.method;
        let uri: string = ctx.req.uri;
        console.log("observe triggered");
        console.log("method", operation);
        console.log("uri", uri);
        if (operation === "POST" && uri === googleParam.google.urlPlaces) {
            console.log("Inserisco logica");
            console.log("Uri", ctx.req.uri);
            //recupero i parametri;
            let adddressParamArray: any = ctx.req.qs.address.split(",");
            let latitudine: string = adddressParamArray[0];
            let longitudine: string = adddressParamArray[1];
            let raggio: string = adddressParamArray[2];
            let tipo: string = adddressParamArray[3];
            let keyWord: string = adddressParamArray[4];
            if (latitudine.length == 0) {
                next("latitudine è un campo obbligatorio");
            }
            if (longitudine.length == 0) {
                next("longitudine è un campo obbligatorio");
            }
            if (raggio.length == 0) {
                raggio = googleParam.google.radius;
            }
            let location: string = "location=" + latitudine + "," + longitudine;
            console.log("location", location);
            console.log("raggio", raggio);
            let stringParameter: string;
            if (tipo.length == 0 && keyWord.length == 0) {
                stringParameter = location + "&radius=" + raggio;
            } else if (tipo.length > 0 && keyWord.length == 0) {
                stringParameter = location + "&radius=" + raggio + "&type=" + tipo;
            } else if (tipo.length == 0 && keyWord.length > 0) {
                stringParameter = location + "&radius=" + raggio + "&keyword=" + keyWord;
            } else {
                stringParameter = location + "&radius=" + raggio + "&type=" + tipo + "&keyword=" + keyWord;
            }
            stringParameter = stringParameter + "&key=" + googleParam.google.apiKey;
            console.log("stringParameter", stringParameter);
            ctx.req.uri = ctx.req.uri.replace("<<PARAMETER_GETPLACES>>", stringParameter);
            console.log("Uri dopo", ctx.req.uri);
        }
        next();
    });
};