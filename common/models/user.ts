'use strict';
var config = require('../../server/config.json');
var datasources = require('../../server/datasources');
var path = require('path');
var app = require(__dirname + '/../../server/server');
let htmlTemplates = require(__dirname +'/../../resources/htmlTemplate');


module.exports = function(User:any) {
    //send verification email after registration
    User.afterRemote('create', function(context:any, userInstance:any, next:any) {
        console.log('> user.afterRemote triggered');

        var objMail = {
            type: 'email',
            to: userInstance.email,
            from: 'noreply@loopback.com',
            subject: 'Thanks for registering.',
             html: 'Conferma la tua email cliccando sul link qui sotto ' +
             'o aprendolo in un browser:<br/><a href={href}> Clicca qui per attivare l\'account </a><br/><br/>' +
             ' Grazie per la collaborazione',//+
            // htmlTemplates.emailFooter,
           // template: path.resolve(__dirname, '../../server/views/verify.ejs'),
            redirect: '/user/verified',
            user: userInstance
        };
        userInstance.verify(objMail, function(err:object, response:object, next:any) {
            if (err) return next(err);
            console.log('> verification email sent:', response);
            context.res.render('verified', {
                title: 'Signed up successfully',
                content: 'Please check your email and click on the verification link before logging in.',
                redirectTo: '/',
                redirectToLinkText: 'Log in'
            });

            app.models.Email.send(objMail, function(err:object) {
                if (err) {
                    console.log('Error sending privacy', err);
                } else {
                    console.log('Sent privacy mail');
                }

            });


        });

    });

 User.on('resetPasswordRequest', function(info:any) {
     console.log ("PASSO IN RESET");
    var sendFrom = datasources.emailDs.transports[0].auth.user;
    var cfg = require('../../server/config');
    var url = 'http://' + cfg.host + ':' + cfg.port + '/users/changepwd';

    var usrId = info.accessToken.userId;
    var tokenId = info.accessToken.id;

    var addr = 'http://' + cfg.host + ':' + cfg.port;
    var htmlText = 'Clicca <a href="' + url + '?id=' +
      usrId + '&token=' + tokenId + '&url=' + addr +
      '">qui</a> per modificare la password' + htmlTemplates.emailFooter;

    var objMail = {
      to: info.email,
      from: sendFrom,
      subject: 'PromoMappa – Richiesta reset password',
      html: htmlText
    };

    app.models.Email.send(objMail, function(err:object) {
      if (err) {
        console.log('send mail reset password err', err);
      } else {
        console.log('send mail reset password success');
      }

    });

  });
};






