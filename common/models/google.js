'use strict';
var app = require(__dirname + '/../../server/server');
var googleParam = require('../../configuration/param');
var _ = require('lodash');
var APIConnector;
var apiKey = googleParam.google.apiKey;
module.exports = function (Google) {
    Google.beforeRemote("**", function (ctx, modelInstance, next) {
        next();
    });
    var isStatic = true;
    Google.disableRemoteMethod('count', isStatic);
    Google.disableRemoteMethod('deleteById', isStatic);
    Google.disableRemoteMethod('create', isStatic);
    Google.disableRemoteMethod('upsert', isStatic);
    Google.disableRemoteMethod('updateAll', isStatic);
    Google.disableRemoteMethod('findOne', isStatic);
    Google.disableRemoteMethod('find', isStatic);
    Google.disableRemoteMethod('findById', isStatic);
    Google.disableRemoteMethod('replaceOrCreate', isStatic);
    Google.disableRemoteMethod('invoke', isStatic);
    Google.disableRemoteMethod('description', isStatic);
    Google.disableRemoteMethod('replaceById', isStatic);
    Google.disableRemoteMethod('upsertWithWhere', isStatic);
    Google.disableRemoteMethod('__exists__', isStatic);
    Google.disableRemoteMethod("__update-__", isStatic);
    Google.disableRemoteMethod('createChangeStream', isStatic);
    APIConnector = app.dataSources.GoogleDS.connector;
    APIConnector.observe('before execute', function (ctx, next) {
        var operation = ctx.req.method;
        var uri = ctx.req.uri;
        console.log("observe triggered");
        console.log("method", operation);
        console.log("uri", uri);
        if (operation === "POST" && uri === googleParam.google.urlPlaces) {
            console.log("Inserisco logica");
            console.log("Uri", ctx.req.uri);
            //recupero i parametri;
            var adddressParamArray = ctx.req.qs.address.split(",");
            var latitudine = adddressParamArray[0];
            var longitudine = adddressParamArray[1];
            var raggio = adddressParamArray[2];
            var tipo = adddressParamArray[3];
            var keyWord = adddressParamArray[4];
            if (latitudine.length == 0) {
                next("latitudine è un campo obbligatorio");
            }
            if (longitudine.length == 0) {
                next("longitudine è un campo obbligatorio");
            }
            if (raggio.length == 0) {
                raggio = googleParam.google.radius;
            }
            var location_1 = "location=" + latitudine + "," + longitudine;
            console.log("location", location_1);
            console.log("raggio", raggio);
            var stringParameter = void 0;
            if (tipo.length == 0 && keyWord.length == 0) {
                stringParameter = location_1 + "&radius=" + raggio;
            }
            else if (tipo.length > 0 && keyWord.length == 0) {
                stringParameter = location_1 + "&radius=" + raggio + "&type=" + tipo;
            }
            else if (tipo.length == 0 && keyWord.length > 0) {
                stringParameter = location_1 + "&radius=" + raggio + "&keyword=" + keyWord;
            }
            else {
                stringParameter = location_1 + "&radius=" + raggio + "&type=" + tipo + "&keyword=" + keyWord;
            }
            stringParameter = stringParameter + "&key=" + googleParam.google.apiKey;
            console.log("stringParameter", stringParameter);
            ctx.req.uri = ctx.req.uri.replace("<<PARAMETER_GETPLACES>>", stringParameter);
            console.log("Uri dopo", ctx.req.uri);
        }
        next();
    });
};
