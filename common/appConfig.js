var appConfig = {
    baseUrl: 'http://localhost:3000',
    paginationParams: {
        limit: 25,
        skip: 0
    },
    activityTimeout: 15 * 60 * 1000, // 15 MINUTES
};

module.exports = appConfig;
