'use strict';

var path = require('path');
var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.MySql;

var lbTables = ['user', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];

ds.autoupdate(lbTables, function(err, result) { // oppure autoupdate oppure automigrate
    if (err) {
        throw err;
    } else {
        console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
        if (ds) {
            console.log('Disconnetto');
            ds.disconnect();
        }
    }

});