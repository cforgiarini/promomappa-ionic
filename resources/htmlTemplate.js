var emailFooter = '<br/><br/><p style'+
    '="font-size: 12.8px;margin:0;padding:0"><b><span ' +
    'style="font-size: 9pt; font-family: arial;">mog</span></b><span ' +
    'style="font-size: 9pt; font-family: arial;">|buone prassi<u></u>'+
    '<u></u></span></p><p style="font-size: 12.8px;margin:0;padding:0">'+
    '<b><span style' +
    '="font-size: 11pt; font-family: arial;">INFORTUNI</span></b><span style'+
    '="font-size: 11pt; font-family: arial;">ZERO</span></p><br/><br/>'+
    '<div id="footerEmail" style="font-size: 12.8px;margin:0;padding:0">'+
    '<p style="font-size: 12.8px;margin:0;padding:0">'+
    '<span style="font-size: 7.5pt; font-family: arial; color: black;">'+
    'Think before you print</span><span style="font-family: helvetica;">'+
    '<u></u><u></u></span></p>'+
    '<p style="font-size: 12.8px;margin:0;padding:0"><i><span lang'+
    '="EN-GB" style="font-size: 11pt; color:rgb(89, 89, 89);"><hr/>'+
    '</span></i><span style="font-family: &quot;times new roman&quot;;"><u>'+
    '</u><u></u></span></p>'+
    '<p style="font-size: 12.8px;margin:0;padding:0"><span style'+
    '="font-size: 9pt; color: rgb(89, 89, 89);">'+
    'Le informazioni contenute nella presente comunicazione e i relativi '+
    'allegati possono essere riservate e sono, comunque, destinate '+
    'esclusivamente alle persone o alla Società sopraindicati. La diffusione,'+
    ' distribuzione e/o copiatura del documento trasmesso da parte di '+
    'qualsiasi soggetto diverso dal destinatario è proibita, sia ai sensi '+
    'dell’art. 616 c.p. , che ai sensi del D.Lgs. n. 196/2003. Se avete '+
    'ricevuto questo messaggio per errore, vi preghiamo di distruggerlo e '+
    'di informarci immediatamente, dandocene gentilmente comunicazione.'+
    '</span><span style="font-family: &quot;times new roman&quot;; '+
    'color: black;"><u></u><u></u></span></p>'+
    '<p><span style="font-size: 9pt; color: rgb(89, 89, 89);">'+
    'Pursuant to Legislative Decree No. 196/2003, you are hereby informed'+
    ' that this message contains confidential information intended only '+
    'for the use of the addressee. If you are not the addressee, and have '+
    'received this message by mistake, please delete it and immediately notify'+
    ' us. You may not copy or disseminate this message to anyone.&nbsp;'+
    'Thank you.</span></p></div>';
// export it
exports.emailFooter = emailFooter;