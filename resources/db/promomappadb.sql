-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              5.7.14 - MySQL Community Server (GPL)
-- S.O. server:                  Win64
-- HeidiSQL Versione:            9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database promomappa
DROP DATABASE IF EXISTS `promomappa`;
CREATE DATABASE IF NOT EXISTS `promomappa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `promomappa`;

-- Dump della struttura di tabella promomappa.accesstoken
DROP TABLE IF EXISTS `accesstoken`;
CREATE TABLE IF NOT EXISTS `accesstoken` (
  `id` varchar(255) NOT NULL,
  `ttl` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella promomappa.accesstoken: 1 rows
DELETE FROM `accesstoken`;
/*!40000 ALTER TABLE `accesstoken` DISABLE KEYS */;
INSERT INTO `accesstoken` (`id`, `ttl`, `created`, `userId`) VALUES
	('Zl2BrKHnxNj5Lu8p7yHhIYVcKgD62n2nge0MLBLhIXSWyaRk9vnzHY5XtpyRAago', 1209600, '2017-05-30 08:24:54', 1);
/*!40000 ALTER TABLE `accesstoken` ENABLE KEYS */;

-- Dump della struttura di tabella promomappa.acl
DROP TABLE IF EXISTS `acl`;
CREATE TABLE IF NOT EXISTS `acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(512) DEFAULT NULL,
  `property` varchar(512) DEFAULT NULL,
  `accessType` varchar(512) DEFAULT NULL,
  `permission` varchar(512) DEFAULT NULL,
  `principalType` varchar(512) DEFAULT NULL,
  `principalId` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella promomappa.acl: 0 rows
DELETE FROM `acl`;
/*!40000 ALTER TABLE `acl` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl` ENABLE KEYS */;

-- Dump della struttura di tabella promomappa.category
DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dump dei dati della tabella promomappa.category: 8 rows
DELETE FROM `category`;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id`, `desc`) VALUES
	(1, 'Benessere'),
	(2, 'Cultura'),
	(3, 'Arte'),
	(4, 'Altro'),
	(5, 'Sanita'),
	(6, 'Tempo libero'),
	(7, 'Servizi'),
	(8, 'Trasporti');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dump della struttura di tabella promomappa.google_place_types_supported
DROP TABLE IF EXISTS `google_place_types_supported`;
CREATE TABLE IF NOT EXISTS `google_place_types_supported` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `google_key` varchar(45) CHARACTER SET latin1 NOT NULL,
  `en` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `it` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `fr` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `de` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT '0',
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_category` (`category_id`),
  CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

-- Dump dei dati della tabella promomappa.google_place_types_supported: ~90 rows (circa)
DELETE FROM `google_place_types_supported`;
/*!40000 ALTER TABLE `google_place_types_supported` DISABLE KEYS */;
INSERT INTO `google_place_types_supported` (`id`, `google_key`, `en`, `it`, `fr`, `de`, `enabled`, `category_id`) VALUES
	(1, 'accounting', 'Accounting', 'Contabilità', '', '', 0, 4),
	(2, 'airport', 'Airport', 'Aeroporto', '', '', 0, 4),
	(3, 'amusement_park', 'Amusement park', 'Parco divertimenti', '', '', 0, 4),
	(4, 'aquarium', 'Aquarium', 'Acquario', '', '', 0, 4),
	(5, 'art_gallery', 'Art gallery', 'Galleria d\'arte', '', '', 1, 2),
	(6, 'atm', 'Atm', 'ATM', '', '', 0, 4),
	(7, 'bakery', 'Bakery', 'Forno', '', '', 0, 4),
	(8, 'bank', 'Bank', 'Banca', '', '', 0, 4),
	(9, 'bar', 'Bar', 'Bar', '', '', 0, 4),
	(10, 'beauty_salon', 'Beauty salon', 'Salone di bellezza', '', '', 1, 1),
	(11, 'bicycle_store', 'Bicycle store', 'Vendita biciclette', '', '', 0, 4),
	(12, 'book_store', 'Book store', 'Libreria', '', '', 0, 4),
	(13, 'bowling_alley', 'Bowling alley', 'Sala da bowling', '', '', 0, 4),
	(14, 'bus_station', 'Bus station', 'Stazione degli autobus', '', '', 0, 4),
	(15, 'cafe', 'Cafe', 'Bar', '', '', 0, 4),
	(16, 'campground', 'Campground', 'Campeggio', '', '', 0, 4),
	(17, 'car_dealer', 'Car dealer', 'Rivenditore d\'auto', '', '', 0, 4),
	(18, 'car_rental', 'Car rental', 'Noleggio auto', '', '', 0, 4),
	(19, 'car_repair', 'Car repair', 'Riparazione auto', '', '', 0, 4),
	(20, 'car_wash', 'Car wash', 'Autolavaggio', '', '', 0, 4),
	(21, 'casino', 'Casino', 'Casinò', '', '', 0, 4),
	(22, 'cemetery', 'Cemetery', 'Cimitero', '', '', 0, 4),
	(23, 'church', 'Church', 'Chiesa', '', '', 0, 4),
	(24, 'city_hall', 'City_hall', 'Municipio', '', '', 0, 4),
	(25, 'clothing_store', 'Clothing_store', 'Negozio di vestiti', '', '', 0, 4),
	(26, 'convenience_store', 'Convenience_store', 'Minimarket', '', '', 0, 4),
	(27, 'courthouse', 'Courthouse', 'Palazzo di giustizia', '', '', 0, 4),
	(28, 'dentist', 'Dentist', 'Dentista', '', '', 1, 5),
	(29, 'department_store', 'Department_store', 'Grande magazzino', '', '', 0, 4),
	(30, 'doctor', 'Doctor', 'Medico', '', '', 1, 5),
	(31, 'electrician', 'Electrician', 'Elettricista', '', '', 0, 4),
	(32, 'electronics_store', 'Electronics_store', 'Negozio di elettronica', '', '', 0, 4),
	(33, 'embassy', 'Embassy', 'Ambasciata', '', '', 0, 4),
	(34, 'fire_station', 'Fire station', 'Caserma dei pompieri', '', '', 0, 4),
	(35, 'florist', 'Florist', 'Fioraio', '', '', 0, 4),
	(36, 'funeral_home', 'Funeral_home', 'Onoranze funebri', '', '', 0, 4),
	(37, 'furniture_store', 'Furniture_store', 'Negozio di mobili', '', '', 0, 4),
	(38, 'gas_station', 'Gas_station', 'Stazione di servizio', '', '', 0, 4),
	(39, 'gym', 'Gym', 'Palestra', '', '', 1, 2),
	(40, 'hair_care', 'Parrucchiere', 'Cura dei capelli', '', '', 1, 1),
	(41, 'hardware_store', 'Hardware_store', 'Negozio hardware', '', '', 0, 4),
	(42, 'hindu_temple', 'Hindu temple', 'Tempio Hindu', '', '', 0, 4),
	(43, 'home_goods_store', 'Home goods_store', 'Negozio di articoli per la casa', '', '', 0, 4),
	(44, 'hospital', 'Hospital', 'Ospedale', '', '', 1, 5),
	(45, 'insurance_agency', 'Insurance agency', 'Agenzia assicurazioni', '', '', 0, 4),
	(46, 'jewelry_store', 'Jewelry store', 'Gioielleria', '', '', 0, 4),
	(47, 'laundry', 'Laundry', 'Lavanderia', '', '', 0, 4),
	(48, 'lawyer', 'Lawyer', 'Avvocato', '', '', 0, 4),
	(49, 'library', 'Library', 'Biblioteca', '', '', 0, 4),
	(50, 'liquor_store', 'Liquor store', 'Negozio di liquori', '', '', 0, 4),
	(51, 'local_government_office', 'Local government office', 'Local_government_office', '', '', 0, 4),
	(52, 'locksmith', 'Locksmith', 'Fabbro ferraio', '', '', 0, 4),
	(53, 'lodging', 'Lodging', 'Alloggio', '', '', 0, 4),
	(54, 'meal_delivery', 'Meal delivery', 'Consegna dei pasti spedizione', '', '', 0, 4),
	(55, 'meal_takeaway', 'Meal takeaway', 'Consegna dei pasti asporto', '', '', 0, 4),
	(56, 'mosque', 'Mosque', 'Moschea', '', '', 0, 4),
	(57, 'movie_rental', 'Movie rental', 'Affitto film', '', '', 0, 4),
	(58, 'movie_theater', 'Movie theater', 'Cinema', '', '', 0, 4),
	(59, 'moving_company', 'Moving company', 'Azienda Traslochi', '', '', 0, 4),
	(60, 'museum', 'Museum', 'Museo', '', '', 1, 2),
	(61, 'night_club', 'Night club', 'Discoteca', '', '', 1, 6),
	(62, 'painter', 'Painter', 'Pittore', '', '', 0, 4),
	(63, 'park', 'Park', 'Parco', '', '', 0, 4),
	(64, 'parking', 'Parking', 'Parcheggio', '', '', 0, 4),
	(65, 'pet_store', 'Petstore', 'Negozio di animali', '', '', 0, 4),
	(66, 'pharmacy', 'Pharmacy', 'Farmacia', '', '', 1, 5),
	(67, 'physiotherapist', 'Physiotherapist', 'Fisioterapista', '', '', 1, 5),
	(68, 'plumber', 'Plumber', 'Idraulico', '', '', 0, 4),
	(69, 'police', 'Police', 'Polizia', '', '', 0, 4),
	(70, 'post_office', 'Post office', 'Ufficio postale', '', '', 0, 4),
	(71, 'real_estate_agency', 'Real estate agency', 'Agenzia immobiliare', '', '', 0, 4),
	(72, 'restaurant', 'Restaurant', 'Ristorante', '', '', 0, 4),
	(73, 'roofing_contractor', 'Roofing contractor', 'Roofing contractor', '', '', 0, 4),
	(74, 'rv_park', 'Rv park', 'Rv park', '', '', 0, 4),
	(75, 'school', 'School', 'Scuola', '', '', 0, 4),
	(76, 'shoe_store', 'Shoe_store', 'Negozio di scarpe', '', '', 0, 4),
	(77, 'shopping_mall', 'Shopping_mall', 'Centro commerciale', '', '', 1, 6),
	(78, 'spa', 'Spa', 'Terme', '', '', 1, 1),
	(79, 'stadium', 'Stadium', 'Stadio', '', '', 0, 4),
	(80, 'storage', 'Storage', 'Conservazione', '', '', 0, 4),
	(81, 'store', 'Store', 'Negozio', '', '', 0, 4),
	(82, 'subway_station', 'Subway station', 'Stazione della metropolitana', '', '', 1, 7),
	(83, 'synagogue', 'Synagogue', 'Sinagoga', '', '', 0, 4),
	(84, 'taxi_stand', 'Taxi stand', 'Taxi stand', '', '', 0, 4),
	(85, 'train_station', 'Train station', 'Stazione ferroviaria', '', '', 0, 8),
	(86, 'transit_station', 'Transit station', 'Stazione di transito', '', '', 0, 4),
	(87, 'travel_agency', 'Travel agency', 'Agenzia di viaggi', '', '', 0, 4),
	(88, 'university', 'University', 'Università', '', '', 0, 4),
	(89, 'veterinary_care', 'Veterinary care', 'Veterinario', '', '', 0, 6),
	(90, 'zoo', 'Zoo', 'Zoo', '', '', 0, 4);
/*!40000 ALTER TABLE `google_place_types_supported` ENABLE KEYS */;

-- Dump della struttura di tabella promomappa.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella promomappa.role: 0 rows
DELETE FROM `role`;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dump della struttura di tabella promomappa.rolemapping
DROP TABLE IF EXISTS `rolemapping`;
CREATE TABLE IF NOT EXISTS `rolemapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `principalType` varchar(512) DEFAULT NULL,
  `principalId` varchar(255) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `principalId` (`principalId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella promomappa.rolemapping: 0 rows
DELETE FROM `rolemapping`;
/*!40000 ALTER TABLE `rolemapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `rolemapping` ENABLE KEYS */;

-- Dump della struttura di tabella promomappa.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `realm` varchar(512) DEFAULT NULL,
  `username` varchar(512) DEFAULT NULL,
  `password` varchar(512) NOT NULL,
  `credentials` text,
  `challenges` text,
  `email` varchar(512) NOT NULL,
  `emailVerified` tinyint(1) DEFAULT NULL,
  `verificationToken` varchar(512) DEFAULT NULL,
  `status` varchar(512) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `lastUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella promomappa.user: 1 rows
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `realm`, `username`, `password`, `credentials`, `challenges`, `email`, `emailVerified`, `verificationToken`, `status`, `created`, `lastUpdated`) VALUES
	(1, NULL, NULL, '$2a$10$OlrDSA2fEr2.UM/TUQxeHuJJICjz04kzdfsYcZjxNFyhxhnjiG68G', 'null', 'null', 'cforgiarini@athirat.com', 1, NULL, NULL, '2017-05-24 13:40:25', '2017-05-24 13:40:25');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

/*
CREO UTENTE APPLICATIVO
*/
CREATE USER 'athirat'@'localhost';
GRANT USAGE ON *.* TO 'athirat'@'localhost';
GRANT EXECUTE, SELECT, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON `promomappa`.* TO 'athirat2'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;
